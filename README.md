# Conduite de Tests

# Environnement de dévellopement :

OS : `Windows 10 x64`,

IDE : `Visual Studio Code`


# Configuration de la base de données et API (si nécessaire)

Dépendances : `php >= 7.3`, `composer >= 1.9`


Remarque : Vérifier que le fichier de configuration `php.ini` situé dans votre dossier d'installation de `PHP` possède bien les deux extensions suivantes décommentées (enlever le ";") :

```
`extension=pdo_sqlite`
`extension=sqlite3`
```

L'API et la base de données utilisent le framework proposé par `api-platform` (symfony).

La base de données est généree avec l'ORM mis à disposition par le framework dans un fichier sqlite3 situé dans `api-shop\var\data.db`. 


Génération et/ou mis à jour de la base de données:

Création de la base de données SQLite (si elle n'existe pas déjà).
`php bin/console doctrine:database:create`

Mise à jour de la base de données:

2 Possibilités:

`php bin/console doctrine:schema:update --force`    (Recommandé)

    ou

Préparation de la migration de la base de données:
`php ./bin/console make:migration` 
Migration de la base de données vers le fichier `data.db`:
`php bin/console --no-interaction doctrine:migrations:migrate`


Démarrage de serveur API sur le port 8000:
`php -S 127.0.0.1:8000 -t public`


# Installation de l'environnement de deployement

Dépendances : `php >= 7.3`, `composer >= 1.9`

Etape essentiel a effectuer après les vérifications de dépendendances et la configuration du php.ini.

Executer la commande suivante dans le dossier `\api-shop` afin de  vérifier les dépendances du projet:

```
composer install
```

Création de la base: lancer le script `\api-shop\create_maj_db.py`.

# Lancement Serveur API/Base de Données

Dépendances :`python >= 3.7`, `php >= 7.3`, `composer >= 1.9`

Remarque : Vérifier que le dossier `api-shop\src\Migrations` est bien vide avant de lancer l'application (sinon des erreurs de versions de migrations peuvent apparaître.


Lancer le script python `\api-shop\exe_api.py` pour créer/mettre à jour la base de données et lancer le serveur de l'API.

# Arrêter Serveur API/Base de Données

L'arrêt de serveur s'effectue à l'aide de la commande `Ctrl+C` depuis la fenêtre d'execution du fichier 


# Serveur Angular

Se place dans le dossier "ShoppApp".

Installer les dépendances avec la commande `npm install`

Lancer l'application avec la commande `npm start`

Le serveur est ouvert sur l'adresse : http://localhost:4200/

# Peuplement de la base de données (avec Jeu de Tests)

Se placer dans le dossier `api-shop` et effectuer la commande suivante :

`php bin/console hautelook:fixtures:load`

# Tests Back (Unitaire et Fonctionnel)

Les classes de tests sont dans le dossier `api-shop/tests` et dans le sous-dossier correspondant au type de test effectué (Fonctionnel/Unitaire)

Un rapport `report_tests` référençant les tests effectués sur le Back est présent dans le dossier `api-shop`.

Lancement des tests : Si vous souhaitez lancer les tests du Back par vous même, voci la procédure à suivre :

Se placer dans le dossier `api-shop` et effectuer la commande suivante :

`vendor/bin/simple-phpunit`

Remarque : _ Il est possible que lors de l'execution de la commande précédente un bug se produise indiquant une erreur 'PHP Fatal error:  Symfony\Component\HttpKernel\Kernel::initializeContainer()...'.
           Si cette erreur se produit, veuillez relancer la commande précédente pour forcer le système à effectuer les tests.
           
           _ Le fichier `vendor/bin/simple-phpunit` est un `.bat`, la commande précédente permettant de lancer les tests fonctionne donc dans le cadre d'un OS Windows.
           Pout tout autre Système d'exploitation veuillez vous réferrer à la documentation correspondante pour executer un `.bat`.

## Available Scripts

In the project directory `\api-shop`, you can run:

### `php -S 127.0.0.1:8000 -t public`

Runs the app in the development mode with back-end.<br>
Open [http://127.0.0.1:8000](http://127.0.0.1:8000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.


## Learn More

You can learn more in the [Create api-platform documentation](https://api-platform.com/docs/distribution).

To learn Symfony, check out the [Symfony documentation](https://symfony.com/doc/current/index.html#gsc.tab=0).

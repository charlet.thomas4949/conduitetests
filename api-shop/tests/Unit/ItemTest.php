<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Doctrine\Common\Collections\ArrayCollection;
use App\Entity\Item;
use App\Entity\Inventory;
use App\Entity\Category;

final class ItemTest extends TestCase
{
    public function testCanBeCreatedFromValidItem(): void
    {
        //Test Constructor Item
        $item = new Item();

        //Test Setters
        $this->assertInstanceOf(Item::class, $item->setRefCode('new_item_refCode'));
        $this->assertInstanceOf(Item::class, $item->setName('new_item_name'));
        $this->assertInstanceOf(Item::class, $item->setDescription('new_item_description'));

        //Test Getters
        $this->assertEquals('new_item_refCode', $item->getRefCode());
        $this->assertEquals('new_item_name', $item->getName());
        $this->assertEquals('new_item_description', $item->getDescription());

    }

    public function testCategoriesItem(): void
    {
        $item = new Item();

        // Test getCategories() 
        $list_categories=$item->getCategories();
        // Test list_categories Collection size 0
        $this->assertEquals(0,count($list_categories));

        // Test addCategory()
        $new_categoy = new Category();
        $item->addCategory($new_categoy);
        // Test list_categories Collection size 1
        $this->assertEquals(1,count($item->getCategories()));

        // Test last object add instance of Category
        $first_category=$item->getCategories()[0];
        $this->assertInstanceOf(Category::class,$first_category);

        // Test removeCategory()
        $item->removeCategory($new_categoy);
        // Test list_categories Collection size 0
        $this->assertEquals(0,count($item->getCategories()));
    }

    public function testInventoriesItem(): void
    {
        $item = new Item();

        // Test getInventories() 
        $list_inventories=$item->getInventories();
        // Test list_inventories Collection size 0
        $this->assertEquals(0,count($list_inventories));

        // Test addInventory()
        $new_inventory = new Inventory();

        $item->addInventory($new_inventory);
        // Test list_inventories Collection size 1
        $this->assertEquals(1,count($item->getInventories()));

        // Test last object add instance of Inventory
        $first_inventory=$item->getInventories()[0];
        $this->assertInstanceOf(Inventory::class,$first_inventory);

        // Test removeInventory()
        $item->removeInventory($new_inventory);
        // Test list_inventories Collection size 0
        $this->assertEquals(0,count($item->getInventories()));
    }
}
<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Doctrine\Common\Collections\ArrayCollection;
use App\Entity\StoreShelf;
use App\Entity\Store;
use App\Entity\Inventory;
use App\Entity\UserCategoryStoreShelf;

final class StoreShelfTest extends TestCase
{
    public function testCanBeCreatedFromValidStoreShelf(): void
    {
        //Test Constructor StoreShelf
        $storeShelf = new StoreShelf();

        //Test Setters
        $this->assertInstanceOf(StoreShelf::class, $storeShelf->setNumShelf(10));
        $this->assertInstanceOf(StoreShelf::class, $storeShelf->setCodeShelf('new_storeShelf_codeShelf'));
        $this->assertInstanceOf(StoreShelf::class, $storeShelf->setName('new_storeShelf_name'));
        $this->assertInstanceOf(StoreShelf::class, $storeShelf->setStore(new Store()));

        //Test Getters
        $this->assertEquals(10, $storeShelf->getNumShelf());
        $this->assertEquals('new_storeShelf_codeShelf', $storeShelf->getCodeShelf());
        $this->assertEquals('new_storeShelf_name', $storeShelf->getName());
        $this->assertEquals(new Store(), $storeShelf->getStore());

    }

    public function testInventoriesStoreShelf(): void
    {
        $storeShelf = new StoreShelf();

        // Test getInventories() 
        $list_inventories=$storeShelf->getInventories();
        // Test list_inventories Collection size 0
        $this->assertEquals(0,count($list_inventories));

        // Test addInventory()
        $new_inventory = new Inventory();
        $storeShelf->addInventory($new_inventory);
        // Test list_inventories Collection size 1
        $this->assertEquals(1,count($storeShelf->getInventories()));

        // Test last object add instance of Inventory
        $first_inventory=$storeShelf->getInventories()[0];
        $this->assertInstanceOf(Inventory::class,$first_inventory);

        // Test removeInventory()
        $storeShelf->removeInventory($new_inventory);
        // Test list_inventories Collection size 0
        $this->assertEquals(0,count($storeShelf->getInventories()));
    }

    public function testuserCategoryStoreShelvesStoreShelf(): void
    {
        $storeShelf = new StoreShelf();

        // Test getUserCategoryStoreShelves() 
        $list_userCategoryStoreShelves=$storeShelf->getUserCategoryStoreShelves();
        // Test list_userCategoryStoreShelves Collection size 0
        $this->assertEquals(0,count($list_userCategoryStoreShelves));

        // Test addUserCategoryStoreShelf()
        $new_userCategoryStoreShelf = new UserCategoryStoreShelf();

        $storeShelf->addUserCategoryStoreShelf($new_userCategoryStoreShelf);
        // Test list_userCategoryStoreShelves Collection size 1
        $this->assertEquals(1,count($storeShelf->getUserCategoryStoreShelves()));

        // Test last object add instance of UserCategoryStoreShelf
        $first_userCategoryStoreShelf=$storeShelf->getUserCategoryStoreShelves()[0];
        $this->assertInstanceOf(UserCategoryStoreShelf::class,$first_userCategoryStoreShelf);

        // Test removeUserCategoryStoreShelf()
        $storeShelf->removeUserCategoryStoreShelf($new_userCategoryStoreShelf);
        // Test list_items Collection size 0
        $this->assertEquals(0,count($storeShelf->getUserCategoryStoreShelves()));
    }
}
<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Doctrine\Common\Collections\ArrayCollection;
use App\Entity\User;
use App\Entity\Role;
use App\Entity\Gender;
use App\Entity\Store;
use App\Entity\UserCategoryStoreShelf;

final class UserTest extends TestCase
{
    public function testCanBeCreatedFromValidUser(): void
    {
        //Test Constructor User
        $user = new User();

        //Test Setters
        $this->assertInstanceOf(User::class, $user->setUserId('new_user_userId'));
        $this->assertInstanceOf(User::class, $user->setFirstName('new_user_firstName'));
        $this->assertInstanceOf(User::class, $user->setLastName('new_user_lastName'));
        $this->assertInstanceOf(User::class, $user->setEmail('new_user_email'));
        $this->assertInstanceOf(User::class, $user->setPassword('new_user_password'));
        $this->assertInstanceOf(User::class, $user->setBirthday(new DateTime('2020-01-31T16:46:54.219Z')));
        $this->assertInstanceOf(User::class, $user->setRole(new Role()));
        $this->assertInstanceOf(User::class, $user->setGender(new Gender()));
        $this->assertInstanceOf(User::class, $user->setStores(new Store()));

        //Test Getters
        $this->assertEquals('new_user_userId', $user->getUserId());
        $this->assertEquals('new_user_firstName', $user->getFirstName());
        $this->assertEquals('new_user_lastName', $user->getLastName());
        $this->assertEquals('new_user_email', $user->getEmail());
        $this->assertEquals('new_user_password', $user->getPassword());
        $this->assertEquals(new DateTime('2020-01-31T16:46:54.219Z'), $user->getBirthday());
        $this->assertEquals(new Role(), $user->getRole());
        $this->assertEquals(new Gender(), $user->getGender());
        $this->assertEquals(new Store(), $user->getStores());

    }

    public function testuserCategoryStoreShelvesUser(): void
    {
        $user = new User();

        // Test getUserCategoryStoreShelves() 
        $list_userCategoryStoreShelves=$user->getUserCategoryStoreShelves();
        // Test list_userCategoryStoreShelves Collection size 0
        $this->assertEquals(0,count($list_userCategoryStoreShelves));

        // Test addUserCategoryStoreShelf()
        $new_userCategoryStoreShelf = new UserCategoryStoreShelf();

        $user->addUserCategoryStoreShelf($new_userCategoryStoreShelf);
        // Test list_userCategoryStoreShelves Collection size 1
        $this->assertEquals(1,count($user->getUserCategoryStoreShelves()));

        // Test last object add instance of UserCategoryStoreShelf
        $first_userCategoryStoreShelf=$user->getUserCategoryStoreShelves()[0];
        $this->assertInstanceOf(UserCategoryStoreShelf::class,$first_userCategoryStoreShelf);

        // Test removeUserCategoryStoreShelf()
        $user->removeUserCategoryStoreShelf($new_userCategoryStoreShelf);
        // Test list_userCategoryStoreShelves Collection size 0
        $this->assertEquals(0,count($user->getUserCategoryStoreShelves()));
    }
}
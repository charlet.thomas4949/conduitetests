<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Doctrine\Common\Collections\ArrayCollection;
use App\Entity\Gender;
use App\Entity\User;

final class GenderTest extends TestCase
{
    public function testCanBeCreatedFromValidGender(): void
    {
        //Test Constructor Gender
        $gender = new Gender();

        //Test Setters
        $this->assertInstanceOf(Gender::class, $gender->setType('new_gender_type'));
        
        //Test Getters
        $this->assertEquals('new_gender_type', $gender->getType());

    }

    public function testUsersGender(): void
    {
        $gender = new Gender();

        // Test getUsers() 
        $list_users=$gender->getUsersByGender();
        // Test list_users Collection size 0
        $this->assertEquals(0,count($list_users));

        // Test addUser()
        $new_user = new User();
        $gender->addUsersByGender($new_user);
        // Test list_users Collection size 1
        $this->assertEquals(1,count($gender->getUsersByGender()));

        // Test last object add instance of User
        $first_user=$gender->getUsersByGender()[0];
        $this->assertInstanceOf(User::class,$first_user);

        // Test removeUser()
        $gender->removeUsersByGender($new_user);
        // Test list_users Collection size 0
        $this->assertEquals(0,count($gender->getUsersByGender()));
    }
}
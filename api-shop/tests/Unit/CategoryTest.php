<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Doctrine\Common\Collections\ArrayCollection;
use App\Entity\Category;
use App\Entity\Item;
use App\Entity\UserCategoryStoreShelf;

final class CategoryTest extends TestCase
{
    public function testCanBeCreatedFromValidCategory(): void
    {
        //Test Constructor Category
        $category = new Category();

        //Test Setters
        $this->assertInstanceOf(Category::class, $category->setName('new_category_name'));
        $this->assertInstanceOf(Category::class, $category->setCodeCategory('new_category_codeCategory'));

        //Test Getters
        $this->assertEquals('new_category_name', $category->getName());
        $this->assertEquals('new_category_codeCategory', $category->getCodeCategory());

    }

    public function testItemsCategory(): void
    {
        $category = new Category();

        // Test getItems() 
        $list_items=$category->getItems();
        // Test list_items Collection size 0
        $this->assertEquals(0,count($list_items));

        // Test addItem()
        $new_item = new Item();
        $category->addItem($new_item);
        // Test list_items Collection size 1
        $this->assertEquals(1,count($category->getItems()));

        // Test last object add instance of Item
        $first_item=$category->getItems()[0];
        $this->assertInstanceOf(Item::class,$first_item);

        // Test removeItem()
        $category->removeItem($new_item);
        // Test list_items Collection size 0
        $this->assertEquals(0,count($category->getItems()));
    }

    public function testuserCategoryStoreShelvesCategory(): void
    {
        $category = new Category();

        // Test getUserCategoryStoreShelves() 
        $list_userCategoryStoreShelves=$category->getUserCategoryStoreShelves();
        // Test list_userCategoryStoreShelves Collection size 0
        $this->assertEquals(0,count($list_userCategoryStoreShelves));

        // Test addUserCategoryStoreShelf()
        $new_userCategoryStoreShelf = new UserCategoryStoreShelf();

        $category->addUserCategoryStoreShelf($new_userCategoryStoreShelf);
        // Test list_userCategoryStoreShelves Collection size 1
        $this->assertEquals(1,count($category->getUserCategoryStoreShelves()));

        // Test last object add instance of UserCategoryStoreShelf
        $first_userCategoryStoreShelf=$category->getUserCategoryStoreShelves()[0];
        $this->assertInstanceOf(UserCategoryStoreShelf::class,$first_userCategoryStoreShelf);

        // Test removeUserCategoryStoreShelf()
        $category->removeUserCategoryStoreShelf($new_userCategoryStoreShelf);
        // Test list_items Collection size 0
        $this->assertEquals(0,count($category->getUserCategoryStoreShelves()));
    }
}
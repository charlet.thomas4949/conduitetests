<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Doctrine\Common\Collections\ArrayCollection;
use App\Entity\Inventory;
use App\Entity\StoreShelf;
use App\Entity\Item;

final class InventoryTest extends TestCase
{
    public function testCanBeCreatedFromValidInventory(): void
    {
        //Test Constructor Inventory
        $inventory = new Inventory();

        //Test Setters
        $this->assertInstanceOf(Inventory::class, $inventory->setNumberItems(10));
        $this->assertInstanceOf(Inventory::class, $inventory->setPrice(10.0));
        $this->assertInstanceOf(Inventory::class, $inventory->setItem(new Item()));
        $this->assertInstanceOf(Inventory::class, $inventory->setStoreShelf(new StoreShelf()));

        //Test Getters
        $this->assertEquals(10, $inventory->getNumberItems());
        $this->assertEquals(10.0, $inventory->getPrice());
        $this->assertEquals(new Item(), $inventory->getItem());
        $this->assertEquals(new StoreShelf(), $inventory->getStoreShelf());

    }
}
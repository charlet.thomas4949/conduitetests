<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Doctrine\Common\Collections\ArrayCollection;
use App\Entity\Store;
use App\Entity\User;
use App\Entity\StoreShelf;

final class StoreTest extends TestCase
{
    public function testCanBeCreatedFromValidStore(): void
    {
        //Test Constructor Store
        $store = new Store();

        //Test Setters
        $this->assertInstanceOf(Store::class, $store->setCodeStore('new_store_codeStore'));
        $this->assertInstanceOf(Store::class, $store->setName('new_store_name'));

        //Test Getters
        $this->assertEquals('new_store_codeStore', $store->getCodeStore());
        $this->assertEquals('new_store_name', $store->getName());

    }

    public function testUsersStore(): void
    {
        $store = new Store();

        // Test getUsers() 
        $list_users=$store->getUsers();
        // Test list_users Collection size 0
        $this->assertEquals(0,count($list_users));

        // Test addUser()
        $new_user = new User();
        $store->addUser($new_user);
        // Test list_users Collection size 1
        $this->assertEquals(1,count($store->getUsers()));

        // Test last object add instance of User
        $first_user=$store->getUsers()[0];
        $this->assertInstanceOf(User::class,$first_user);

        // Test removeUser()
        $store->removeUser($new_user);
        // Test list_users Collection size 0
        $this->assertEquals(0,count($store->getUsers()));
    }

    public function testuserStoreShelvesStore(): void
    {
        $store = new Store();

        // Test getStoreShelves() 
        $list_storeShelves=$store->getStoreShelves();
        // Test list_storeShelves Collection size 0
        $this->assertEquals(0,count($list_storeShelves));

        // Test addStoreShelf()
        $new_storeShelf = new StoreShelf();

        $store->addStoreShelf($new_storeShelf);
        // Test list_storeShelves Collection size 1
        $this->assertEquals(1,count($store->getStoreShelves()));

        // Test last object add instance of UserCategoryStoreShelf
        $first_storeShelf=$store->getStoreShelves()[0];
        $this->assertInstanceOf(StoreShelf::class,$first_storeShelf);

        // Test removeStoreShelf()
        $store->removeStoreShelf($new_storeShelf);
        // Test list_storeShelves Collection size 0
        $this->assertEquals(0,count($store->getStoreShelves()));
    }
}
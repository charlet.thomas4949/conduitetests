<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Doctrine\Common\Collections\ArrayCollection;
use App\Entity\Role;
use App\Entity\User;

final class RoleTest extends TestCase
{
    public function testCanBeCreatedFromValidRole(): void
    {
        //Test Constructor Role
        $role = new Role();

        //Test Setters
        $this->assertInstanceOf(Role::class, $role->setName('new_role_name'));
        
        //Test Getters
        $this->assertEquals('new_role_name', $role->getName());

    }

    public function testUsersRole(): void
    {
        $role = new Role();

        // Test getUsers() 
        $list_users=$role->getUsers();
        // Test list_users Collection size 0
        $this->assertEquals(0,count($list_users));

        // Test addUser()
        $new_user = new User();
        $role->addUser($new_user);
        // Test list_users Collection size 1
        $this->assertEquals(1,count($role->getUsers()));

        // Test last object add instance of User
        $first_user=$role->getUsers()[0];
        $this->assertInstanceOf(User::class,$first_user);

        // Test removeUser()
        $role->removeUser($new_user);
        // Test list_users Collection size 0
        $this->assertEquals(0,count($role->getUsers()));
    }
}
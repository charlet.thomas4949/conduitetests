<?php

// api/tests/CategoriesTest.php

namespace App\Tests;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\Category;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;

class CategoriesTest extends ApiTestCase
{
    // This trait provided by HautelookAliceBundle will take care of refreshing the database content to a known state before each test
    
    //use RefreshDatabaseTrait;

    public function testGetCollection(): void
    {
        // The client implements Symfony HttpClient's `HttpClientInterface`, and the response `ResponseInterface`
        $response = static::createClient()->request('GET', 'api/categories');

        $this->assertResponseIsSuccessful();
        // Asserts that the returned content type is JSON-LD (the default)
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        // Because test fixtures are automatically loaded between each test, you can assert on them
        //$this->assertCount(8, $response->toArray()['hydra:member']);

        // Asserts that the returned JSON is validated by the JSON Schema generated for this resource by API Platform
        // This generated JSON Schema is also used in the OpenAPI spec!
        $this->assertMatchesResourceCollectionJsonSchema(Category::class);
    }

    public function testCreateCategory(): void
    {
        $response = static::createClient()->request('POST', 'api/categories', ['json' => [
            "name"=> "new_category_name",
            "codeCategory"=> "new_category_codeCategory",
            "items"=> [],
            "userCategoryStoreShelves"=> []
        ]]);

        $this->assertResponseStatusCodeSame(201);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        
        $this->assertRegExp('~^/api/categories/\d+$~', $response->toArray()['@id']);
        $this->assertMatchesResourceItemJsonSchema(Category::class);
    }

    public function testCreateInvalidCategory(): void
    {
        static::createClient()->request('POST', 'api/categories', ['json' => [
            'name' => NULL,
        ]]);

        $this->assertResponseStatusCodeSame(400);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');

        $this->assertMatchesResourceItemJsonSchema(Category::class);
    }

    public function testUpdateCategory(): void
    {
        $client = static::createClient();
        // findIriBy allows to retrieve the IRI of an item by searching for some of its properties.
        $iri = static::findIriBy(Category::class, ['name' => 'new_category_name']);

        $client->request('PUT', $iri, ['json' => [
            'name' => 'new_category_update_name',
        ]]);
        $this->assertResponseIsSuccessful();
        $this->assertMatchesResourceItemJsonSchema(Category::class);
    }

    public function testDeleteCategory(): void
    {

        $client = static::createClient();

        $iri = static::findIriBy(Category::class, ["name" => "new_category_update_name"]);
        $client->request('DELETE', $iri);
        $this->assertResponseStatusCodeSame(204);

    }
}
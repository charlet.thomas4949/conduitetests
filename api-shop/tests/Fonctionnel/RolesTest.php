<?php

// api/tests/RolesTest.php

namespace App\Tests;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\Role;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;

class RolesTest extends ApiTestCase
{
    // This trait provided by HautelookAliceBundle will take care of refreshing the database content to a known state before each test
    
    //use RefreshDatabaseTrait;

    public function testGetCollection(): void
    {
        // The client implements Symfony HttpClient's `HttpClientInterface`, and the response `ResponseInterface`
        $response = static::createClient()->request('GET', 'api/roles');

        $this->assertResponseIsSuccessful();
        // Asserts that the returned content type is JSON-LD (the default)
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        // Because test fixtures are automatically loaded between each test, you can assert on them
        //$this->assertCount(8, $response->toArray()['hydra:member']);

        // Asserts that the returned JSON is validated by the JSON Schema generated for this resource by API Platform
        // This generated JSON Schema is also used in the OpenAPI spec!
        $this->assertMatchesResourceCollectionJsonSchema(Role::class);
    }

    public function testCreateRole(): void
    {
        $response = static::createClient()->request('POST', 'api/roles', ['json' => [
            "name"=> "new_role_name",
        ]]);

        $this->assertResponseStatusCodeSame(201);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        
        $this->assertRegExp('~^/api/roles/\d+$~', $response->toArray()['@id']);
        $this->assertMatchesResourceItemJsonSchema(Role::class);
    }

    public function testCreateInvalidRole(): void
    {
        static::createClient()->request('POST', 'api/roles', ['json' => [
            "name"=> NULL,
        ]]);

        $this->assertResponseStatusCodeSame(400);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');

        $this->assertMatchesResourceItemJsonSchema(Role::class);
    }

    public function testUpdateRole(): void
    {
        $client = static::createClient();
        // findIriBy allows to retrieve the IRI of an item by searching for some of its properties.
        $iri = static::findIriBy(Role::class, ['name' => 'new_role_name']);

        $client->request('PUT', $iri, ['json' => [
            'name' => 'new_role_update_name',
        ]]);
        $this->assertResponseIsSuccessful();
        $this->assertMatchesResourceItemJsonSchema(Role::class);
    }

    public function testDeleteRole(): void
    {

        $client = static::createClient();

        $iri = static::findIriBy(Role::class, ["name" => "new_role_update_name"]);
        $client->request('DELETE', $iri);
        $this->assertResponseStatusCodeSame(204);

    }
}
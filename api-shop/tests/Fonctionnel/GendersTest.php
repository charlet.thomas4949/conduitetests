<?php

// api/tests/GendersTest.php

namespace App\Tests;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\Gender;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;

class GendersTest extends ApiTestCase
{
    // This trait provided by HautelookAliceBundle will take care of refreshing the database content to a known state before each test
    
    //use RefreshDatabaseTrait;

    public function testGetCollection(): void
    {
        // The client implements Symfony HttpClient's `HttpClientInterface`, and the response `ResponseInterface`
        $response = static::createClient()->request('GET', 'api/genders');

        $this->assertResponseIsSuccessful();
        // Asserts that the returned content type is JSON-LD (the default)
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        // Because test fixtures are automatically loaded between each test, you can assert on them
        //$this->assertCount(8, $response->toArray()['hydra:member']);

        // Asserts that the returned JSON is validated by the JSON Schema generated for this resource by API Platform
        // This generated JSON Schema is also used in the OpenAPI spec!
        $this->assertMatchesResourceCollectionJsonSchema(Gender::class);
    }

    public function testCreateGender(): void
    {
        $response = static::createClient()->request('POST', 'api/genders', ['json' => [
            "type"=> "new_gender_typeGender",
            "usersByGender"=> []
        ]]);

        $this->assertResponseStatusCodeSame(201);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        
        $this->assertRegExp('~^/api/genders/\d+$~', $response->toArray()['@id']);
        $this->assertMatchesResourceItemJsonSchema(Gender::class);
    }

    public function testCreateInvalidGender(): void
    {
        static::createClient()->request('POST', 'api/genders', ['json' => [
            'type' => NULL,
        ]]);

        $this->assertResponseStatusCodeSame(400);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');

        $this->assertMatchesResourceItemJsonSchema(Gender::class);
    }

    public function testUpdateGender(): void
    {
        $client = static::createClient();
        // findIriBy allows to retrieve the IRI of an item by searching for some of its properties.
        $iri = static::findIriBy(Gender::class, ['type' => 'new_gender_typeGender']);

        $client->request('PUT', $iri, ['json' => [
            'type' => 'new_gender_update_type',
        ]]);
        $this->assertResponseIsSuccessful();
        $this->assertMatchesResourceItemJsonSchema(Gender::class);
    }

    public function testDeleteGender(): void
    {

        $client = static::createClient();

        $iri = static::findIriBy(Gender::class, ["type" => "new_gender_update_type"]);
        $client->request('DELETE', $iri);
        $this->assertResponseStatusCodeSame(204);

    }
}
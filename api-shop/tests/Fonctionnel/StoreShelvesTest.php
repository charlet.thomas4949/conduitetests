<?php

// api/tests/StoreShelvesTest.php

namespace App\Tests;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\StoreShelf;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;

class StoreShelvesTest extends ApiTestCase
{
    // This trait provided by HautelookAliceBundle will take care of refreshing the database content to a known state before each test
    
    //use RefreshDatabaseTrait;

    public function testGetCollection(): void
    {
        // The client implements Symfony HttpClient's `HttpClientInterface`, and the response `ResponseInterface`
        $response = static::createClient()->request('GET', 'api/store_shelves');

        $this->assertResponseIsSuccessful();
        // Asserts that the returned content type is JSON-LD (the default)
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        // Because test fixtures are automatically loaded between each test, you can assert on them
        //$this->assertCount(8, $response->toArray()['hydra:member']);

        // Asserts that the returned JSON is validated by the JSON Schema generated for this resource by API Platform
        // This generated JSON Schema is also used in the OpenAPI spec!
        $this->assertMatchesResourceCollectionJsonSchema(StoreShelf::class);
    }

    public function testCreateStoreShelf(): void
    {
        $response = static::createClient()->request('POST', 'api/store_shelves', ['json' => [
            "numShelf"=> 0,
            "codeShelf"=> "new_storeShelf_codeShelf",
            "name"=> "new_storeShelf_name",
            "inventories"=> [],
            "userCategoryStoreShelves"=> []
        ]]);

        $this->assertResponseStatusCodeSame(201);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        
        $this->assertRegExp('~^/api/store_shelves/\d+$~', $response->toArray()['@id']);
        $this->assertMatchesResourceItemJsonSchema(StoreShelf::class);
    }

    public function testCreateInvalidStoreShelf(): void
    {
        static::createClient()->request('POST', 'api/store_shelves', ['json' => [
            "numShelf"=> 0,
            "codeShelf"=> "new_storeShelf_codeShelf",
            "name"=> "new_storeShelf_name",
            "inventories"=> NULL,
            "userCategoryStoreShelves"=> NULL
        ]]);

        $this->assertResponseStatusCodeSame(400);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');

        $this->assertMatchesResourceItemJsonSchema(StoreShelf::class);
    }

    public function testUpdateStoreShelf(): void
    {
        $client = static::createClient();
        // findIriBy allows to retrieve the IRI of an item by searching for some of its properties.
        $iri = static::findIriBy(StoreShelf::class, ['name' => 'new_storeShelf_name']);

        $client->request('PUT', $iri, ['json' => [
            'name' => 'new_storeShelf_update_name',
        ]]);
        $this->assertResponseIsSuccessful();
        $this->assertMatchesResourceItemJsonSchema(StoreShelf::class);
    }

    public function testDeleteStoreShelf(): void
    {

        $client = static::createClient();

        $iri = static::findIriBy(StoreShelf::class, ["name" => "new_storeShelf_update_name"]);
        $client->request('DELETE', $iri);
        $this->assertResponseStatusCodeSame(204);

    }
}
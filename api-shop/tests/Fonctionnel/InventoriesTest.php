<?php

// api/tests/InventoriesTest.php

namespace App\Tests;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\Inventory;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;

class InventoriesTest extends ApiTestCase
{
    // This trait provided by HautelookAliceBundle will take care of refreshing the database content to a known state before each test
    
    //use RefreshDatabaseTrait;

    public function testGetCollection(): void
    {
        // The client implements Symfony HttpClient's `HttpClientInterface`, and the response `ResponseInterface`
        $response = static::createClient()->request('GET', 'api/inventories');

        $this->assertResponseIsSuccessful();
        // Asserts that the returned content type is JSON-LD (the default)
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        // Because test fixtures are automatically loaded between each test, you can assert on them
        //$this->assertCount(8, $response->toArray()['hydra:member']);

        // Asserts that the returned JSON is validated by the JSON Schema generated for this resource by API Platform
        // This generated JSON Schema is also used in the OpenAPI spec!
        $this->assertMatchesResourceCollectionJsonSchema(Inventory::class);
    }

    public function testCreateInventory(): void
    {
        $response = static::createClient()->request('POST', 'api/inventories', ['json' => [
            "numberItems"=> 0,
            "price"=> 999999999999.0,
        ]]);

        $this->assertResponseStatusCodeSame(201);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        
        $this->assertRegExp('~^/api/inventories/\d+$~', $response->toArray()['@id']);
        $this->assertMatchesResourceItemJsonSchema(Inventory::class);
    }

    public function testCreateInvalidInventory(): void
    {
        static::createClient()->request('POST', 'api/inventories', ['json' => [
            'numberItems' => NULL,
            'price' => NULL,
        ]]);

        $this->assertResponseStatusCodeSame(400);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');

        $this->assertMatchesResourceItemJsonSchema(Inventory::class);
    }

    public function testUpdateInventory(): void
    {
        $client = static::createClient();
        // findIriBy allows to retrieve the IRI of an item by searching for some of its properties.
        $iri = static::findIriBy(Inventory::class, ['numberItems' => 0,"price"=> 999999999999.0]);

        $client->request('PUT', $iri, ['json' => [
            'numberItems' => 99999999999,
            "price"=> 99999999999
        ]]);
        $this->assertResponseIsSuccessful();
        $this->assertMatchesResourceItemJsonSchema(Inventory::class);
    }

    public function testDeleteInventory(): void
    {

        $client = static::createClient();

        $iri = static::findIriBy(Inventory::class, ['numberItems' => 99999999999,"price"=> 99999999999]);
        $client->request('DELETE', $iri);
        $this->assertResponseStatusCodeSame(204);

    }
}
<?php

// api/tests/StoresTest.php

namespace App\Tests;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\Store;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;

class StoresTest extends ApiTestCase
{
    // This trait provided by HautelookAliceBundle will take care of refreshing the database content to a known state before each test
    
    //use RefreshDatabaseTrait;

    public function testGetCollection(): void
    {
        // The client implements Symfony HttpClient's `HttpClientInterface`, and the response `ResponseInterface`
        $response = static::createClient()->request('GET', 'api/stores');

        $this->assertResponseIsSuccessful();
        // Asserts that the returned content type is JSON-LD (the default)
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        // Because test fixtures are automatically loaded between each test, you can assert on them
        //$this->assertCount(8, $response->toArray()['hydra:member']);

        // Asserts that the returned JSON is validated by the JSON Schema generated for this resource by API Platform
        // This generated JSON Schema is also used in the OpenAPI spec!
        $this->assertMatchesResourceCollectionJsonSchema(Store::class);
    }

    public function testCreateStore(): void
    {
        $response = static::createClient()->request('POST', 'api/stores', ['json' => [
            "codeStore"=> "new_store_codeStore",
            "name"=> "new_store_name",
            "users"=> [],
            "storeShelves"=> []
        ]]);

        $this->assertResponseStatusCodeSame(201);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        
        $this->assertRegExp('~^/api/stores/\d+$~', $response->toArray()['@id']);
        $this->assertMatchesResourceItemJsonSchema(Store::class);
    }

    public function testCreateInvalidStore(): void
    {
        static::createClient()->request('POST', 'api/stores', ['json' => [
            "codeStore"=> "new_store_codeStore",
            "name"=> "new_store_name",
            "users"=> NULL,
            "storeShelves"=> NULL
        ]]);

        $this->assertResponseStatusCodeSame(400);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');

        $this->assertMatchesResourceItemJsonSchema(Store::class);
    }

    public function testUpdateStore(): void
    {
        $client = static::createClient();
        // findIriBy allows to retrieve the IRI of an item by searching for some of its properties.
        $iri = static::findIriBy(Store::class, ['name' => 'new_store_name']);

        $client->request('PUT', $iri, ['json' => [
            'name' => 'new_store_update_name',
        ]]);
        $this->assertResponseIsSuccessful();
        $this->assertMatchesResourceItemJsonSchema(Store::class);
    }

    public function testDeleteStore(): void
    {

        $client = static::createClient();

        $iri = static::findIriBy(Store::class, ["name" => "new_store_update_name"]);
        $client->request('DELETE', $iri);
        $this->assertResponseStatusCodeSame(204);

    }
}
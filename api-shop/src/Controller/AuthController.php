<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class AuthController extends AbstractController
{

    /** @var UserRepository $userRepository */
    private $userRepository;

     /**
     * AuthController Constructor
     *
     * @param UserRepository $usersRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Login new user
     * @param Request $request
     *
     * @return Response
     */
    public function login(Request $request)
    {
        $response =  new Response(sprintf(''));
        $newUserData['email']    = $request->get('email');
        $newUserData['password'] = $request->get('password');

        $user = $this->userRepository->getUser($newUserData);
        $serializer = $this->get('serializer');


        if(empty($user)){
            $response =  new Response(sprintf('Les identifiants ne sont pas corrects !'));
        }else{
            $responseJson = $serializer->serialize($user[0]->getId(),'json');
            $response = new Response(sprintf($responseJson));
        }
        return $response;
    }


    /**
     * @Route("/login/{id}", name="product_show")
     */
    #public function show($id)
    #{
    #    $product = $this->getDoctrine()
    #        ->getRepository(Product::class)
    #        ->find($id);

    #   if (!$product) {
    #        throw $this->createNotFoundException(
    #            'No product found for id '.$id
    #        );
    #    }

    #    return new Response('Check out this great product: '.$product->getName());
    #}


    /**
     * @Route("/auth", name="auth")
     */
    public function index()
    {
        return $this->render('auth/index.html.twig', [
            'controller_name' => 'AuthController',
        ]);
    }

}

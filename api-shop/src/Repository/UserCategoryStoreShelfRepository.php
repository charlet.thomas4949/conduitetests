<?php

namespace App\Repository;

use App\Entity\UserCategoryStoreShelf;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method UserCategoryStoreShelf|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserCategoryStoreShelf|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserCategoryStoreShelf[]    findAll()
 * @method UserCategoryStoreShelf[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserCategoryStoreShelfRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserCategoryStoreShelf::class);
    }

    // /**
    //  * @return UserCategoryStoreShelf[] Returns an array of UserCategoryStoreShelf objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserCategoryStoreShelf
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

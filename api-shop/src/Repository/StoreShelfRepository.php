<?php

namespace App\Repository;

use App\Entity\StoreShelf;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method StoreShelf|null find($id, $lockMode = null, $lockVersion = null)
 * @method StoreShelf|null findOneBy(array $criteria, array $orderBy = null)
 * @method StoreShelf[]    findAll()
 * @method StoreShelf[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StoreShelfRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StoreShelf::class);
    }

    // /**
    //  * @return StoreShelf[] Returns an array of StoreShelf objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?StoreShelf
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

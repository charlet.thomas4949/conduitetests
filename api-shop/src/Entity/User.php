<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use App\Entity\Gender;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use ApiPlatform\Core\Annotation\ApiProperty;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     iri="http://schema.org/User",
 *     normalizationContext={"groups": {"user:read"}})
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ApiFilter(SearchFilter::class, properties={"first_name", "last_name","stores","role","gender"})
 * 
 */
class User
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"user:read","category:read","gender:read", "role:read", "store:read", "storeShelf:read", "userCategoryStoreShelf:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"user:read"})
     */
    private $userId;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"user:read","category:read","gender:read", "role:read", "store:read", "storeShelf:read","userCategoryStoreShelf:read"})
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"user:read","category:read","gender:read", "role:read", "store:read", "storeShelf:read","userCategoryStoreShelf:read"})
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"user:read","category:read","gender:read","role:read"})
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"user:read"})
     */
    private $password;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Groups({"user:read"})
     */
    private $birthday;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Role", inversedBy="users")
     * @ORM\JoinColumn(nullable=true)
     * @Groups({"user:read","category:read","gender:read", "store:read", "storeShelf:read","userCategoryStoreShelf:read"})
     */
    private $role;

    /**
     * @var Gender
     * @ORM\ManyToOne(targetEntity=App\Entity\Gender::class, inversedBy="usersByGender")
     * @ORM\JoinColumn(nullable=true)
     * @Groups({"user:read", "role:read"})
     * 
     */
    private $gender;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Store", inversedBy="users")
     * @Groups({"user:read", "role:read"})
     */
    private $stores;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserCategoryStoreShelf", mappedBy="user")
     * @Groups({"user:read"})
     */
    private $userCategoryStoreShelves;

    public function __construct()
    {
        $this->userCategoryStoreShelves = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserId(): ?string
    {
        return $this->userId;
    }

    public function setUserId(string $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getBirthday(): ?\DateTimeInterface
    {
        return $this->birthday;
    }

    public function setBirthday(?\DateTimeInterface $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }

    public function getRole(): ?Role
    {
        return $this->role;
    }

    public function setRole(?Role $role): self
    {
        $this->role = $role;

        return $this;
    }

    public function getGender(): ?Gender
    {
        return $this->gender;
    }

    public function setGender(?Gender $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getStores(): ?store
    {
        return $this->stores;
    }

    public function setStores(?store $stores): self
    {
        $this->stores = $stores;

        return $this;
    }

    /**
     * @return Collection|UserCategoryStoreShelf[]
     */
    public function getUserCategoryStoreShelves(): Collection
    {
        return $this->userCategoryStoreShelves;
    }

    public function addUserCategoryStoreShelf(UserCategoryStoreShelf $userCategoryStoreShelf): self
    {
        if (!$this->userCategoryStoreShelves->contains($userCategoryStoreShelf)) {
            $this->userCategoryStoreShelves[] = $userCategoryStoreShelf;
            $userCategoryStoreShelf->setUser($this);
        }

        return $this;
    }

    public function removeUserCategoryStoreShelf(UserCategoryStoreShelf $userCategoryStoreShelf): self
    {
        if ($this->userCategoryStoreShelves->contains($userCategoryStoreShelf)) {
            $this->userCategoryStoreShelves->removeElement($userCategoryStoreShelf);
            // set the owning side to null (unless already changed)
            if ($userCategoryStoreShelf->getUser() === $this) {
                $userCategoryStoreShelf->setUser(null);
            }
        }

        return $this;
    }

 
}

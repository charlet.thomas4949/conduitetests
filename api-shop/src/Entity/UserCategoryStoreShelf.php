<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(iri="http://schema.org/UserCategoryStoreShelf",
 *     normalizationContext={"groups": {"userCategoryStoreShelf:read"}})
 * @ORM\Entity(repositoryClass="App\Repository\UserCategoryStoreShelfRepository")
 */
class UserCategoryStoreShelf
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"category:read","storeShelf:read", "store:read","userCategoryStoreShelf:read","storeShelf:read", "user:read"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="userCategoryStoreShelves")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"userCategoryStoreShelf:read","category:read","storeShelf:read","store:read", "storeShelf:read"})
     */
    private $user;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="userCategoryStoreShelves")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"storeShelf:read", "store:read","userCategoryStoreShelf:read", "storeShelf:read", "user:read"})
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\StoreShelf", inversedBy="userCategoryStoreShelves")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"category:read","userCategoryStoreShelf:read", "user:read"})
     */
    private $storeShelf;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?user
    {
        return $this->user;
    }

    public function setUser(?user $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCategory(): ?category
    {
        return $this->category;
    }

    public function setCategory(?category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getStoreShelf(): ?storeShelf
    {
        return $this->storeShelf;
    }

    public function setStoreShelf(?storeShelf $storeShelf): self
    {
        $this->storeShelf = $storeShelf;

        return $this;
    }
}

<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(iri="http://schema.org/Category",
 *     normalizationContext={"groups": {"category:read"}})
 * @ORM\Entity(repositoryClass="App\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"category:read","inventory:read","item:read", "store:read", "storeShelf:read","user:read", "userCategoryStoreShelf:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"category:read","inventory:read","item:read", "store:read", "storeShelf:read","user:read", "userCategoryStoreShelf:read"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"category:read"})
     */
    private $codeCategory;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Item", inversedBy="categories")
     * @Groups({"category:read"})
     */
    private $items;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserCategoryStoreShelf", mappedBy="category")
     * @Groups({"category:read"})
     */
    private $userCategoryStoreShelves;

    public function __construct()
    {
        $this->items = new ArrayCollection();
        $this->userCategoryStoreShelves = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCodeCategory(): ?string
    {
        return $this->codeCategory;
    }

    public function setCodeCategory(?string $codeCategory): self
    {
        $this->codeCategory = $codeCategory;

        return $this;
    }

    /**
     * @return Collection|Item[]
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    public function addItem(Item $item): self
    {
        if (!$this->items->contains($item)) {
            $this->items[] = $item;
        }

        return $this;
    }

    public function removeItem(Item $item): self
    {
        if ($this->items->contains($item)) {
            $this->items->removeElement($item);
        }

        return $this;
    }

    /**
     * @return Collection|UserCategoryStoreShelf[]
     */
    public function getUserCategoryStoreShelves(): Collection
    {
        return $this->userCategoryStoreShelves;
    }

    public function addUserCategoryStoreShelf(UserCategoryStoreShelf $userCategoryStoreShelf): self
    {
        if (!$this->userCategoryStoreShelves->contains($userCategoryStoreShelf)) {
            $this->userCategoryStoreShelves[] = $userCategoryStoreShelf;
            $userCategoryStoreShelf->setCategory($this);
        }

        return $this;
    }

    public function removeUserCategoryStoreShelf(UserCategoryStoreShelf $userCategoryStoreShelf): self
    {
        if ($this->userCategoryStoreShelves->contains($userCategoryStoreShelf)) {
            $this->userCategoryStoreShelves->removeElement($userCategoryStoreShelf);
            // set the owning side to null (unless already changed)
            if ($userCategoryStoreShelf->getCategory() === $this) {
                $userCategoryStoreShelf->setCategory(null);
            }
        }

        return $this;
    }
}

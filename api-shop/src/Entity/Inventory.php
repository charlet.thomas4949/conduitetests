<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(iri="http://schema.org/Inventory",
 *     normalizationContext={"groups": {"inventory:read"}})
 * @ORM\Entity(repositoryClass="App\Repository\InventoryRepository")
 */
class Inventory
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"inventory:read","item:read","store:read", "storeShelf:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"inventory:read","item:read", "store:read", "storeShelf:read"})
     */
    private $numberItems;

    /**
     * @ORM\Column(type="float")
     * @Groups({"inventory:read","item:read", "storeShelf:read"})
     */
    private $price;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Item", inversedBy="inventories")
     * 
     * @Groups({"inventory:read","store:read","storeShelf:read"})
     */
    ///////@ORM\JoinColumn(nullable=false)
    private $item;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\StoreShelf", inversedBy="inventories")
     * 
     * @Groups({"inventory:read","item:read"})
     */
    ///////@ORM\JoinColumn(nullable=false)
    private $storeShelf;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumberItems(): ?int
    {
        return $this->numberItems;
    }

    public function setNumberItems(?int $numberItems): self
    {
        $this->numberItems = $numberItems;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getItem(): ?item
    {
        return $this->item;
    }

    public function setItem(?item $item): self
    {
        $this->item = $item;

        return $this;
    }

    public function getStoreShelf(): ?StoreShelf
    {
        return $this->storeShelf;
    }

    public function setStoreShelf(?StoreShelf $storeShelf): self
    {
        $this->storeShelf = $storeShelf;

        return $this;
    }

    
}

<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(iri="http://schema.org/Gender",
 *     normalizationContext={"groups": {"gender:read"}})
 * @ORM\Entity(repositoryClass="App\Repository\GenderRepository")
 */
class Gender
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"gender:read", "user:read", "role:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"gender:read", "user:read", "role:read"})
     */
    private $type;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\User", mappedBy="gender")
     * @Groups({"gender:read"})
     */
    private $usersByGender;

    public function __construct()
    {
        $this->usersByGender = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsersByGender(): Collection
    {
        return $this->usersByGender;
    }

    public function addUsersByGender(User $usersByGender): self
    {
        if (!$this->usersByGender->contains($usersByGender)) {
            $this->usersByGender[] = $usersByGender;
            $usersByGender->setGender($this);
        }

        return $this;
    }

    public function removeUsersByGender(User $usersByGender): self
    {
        if ($this->usersByGender->contains($usersByGender)) {
            $this->usersByGender->removeElement($usersByGender);
            // set the owning side to null (unless already changed)
            if ($usersByGender->getGender() === $this) {
                $usersByGender->setGender(null);
            }
        }

        return $this;
    }
}

<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(iri="http://schema.org/StoreShelf",
 *     normalizationContext={"groups": {"storeShelf:read"}})
 * @ORM\Entity(repositoryClass="App\Repository\StoreShelfRepository")
 */
class StoreShelf
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"storeShelf:read","category:read", "inventory:read", "item:read", "store:read","user:read", "userCategoryStoreShelf:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"storeShelf:read", "store:read"})
     */
    private $numShelf;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"storeShelf:read", "store:read", })
     */
    private $codeShelf;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"storeShelf:read","category:read", "inventory:read", "item:read", "store:read","user:read", "userCategoryStoreShelf:read"})
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Store", inversedBy="storeShelves")
     * 
     * @Groups({"storeShelf:read", "category:read", "inventory:read", "item:read", "userCategoryStoreShelf:read"})
     */
    /////////////@ORM\JoinColumn(nullable=false)
    private $store;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Inventory", mappedBy="storeShelf")
     * @Groups({"storeShelf:read", "store:read"})
     */
    private $inventories;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserCategoryStoreShelf", mappedBy="storeShelf")
     * @Groups({"storeShelf:read", "store:read"})
     */
    private $userCategoryStoreShelves;

    public function __construct()
    {
        $this->inventories = new ArrayCollection();
        $this->userCategoryStoreShelves = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumShelf(): ?int
    {
        return $this->numShelf;
    }

    public function setNumShelf(?int $numShelf): self
    {
        $this->numShelf = $numShelf;

        return $this;
    }

    public function getCodeShelf(): ?string
    {
        return $this->codeShelf;
    }

    public function setCodeShelf(?string $codeShelf): self
    {
        $this->codeShelf = $codeShelf;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getStore(): ?store
    {
        return $this->store;
    }

    public function setStore(?store $store): self
    {
        $this->store = $store;

        return $this;
    }

    /**
     * @return Collection|inventory[]
     */
    public function getInventories(): Collection
    {
        return $this->inventories;
    }

    public function addInventory(inventory $inventory): self
    {
        if (!$this->inventories->contains($inventory)) {
            $this->inventories[] = $inventory;
            $inventory->setStoreShelf($this);
        }

        return $this;
    }

    public function removeInventory(inventory $inventory): self
    {
        if ($this->inventories->contains($inventory)) {
            $this->inventories->removeElement($inventory);
            // set the owning side to null (unless already changed)
            if ($inventory->getStoreShelf() === $this) {
                $inventory->setStoreShelf(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserCategoryStoreShelf[]
     */
    public function getUserCategoryStoreShelves(): Collection
    {
        return $this->userCategoryStoreShelves;
    }

    public function addUserCategoryStoreShelf(UserCategoryStoreShelf $userCategoryStoreShelf): self
    {
        if (!$this->userCategoryStoreShelves->contains($userCategoryStoreShelf)) {
            $this->userCategoryStoreShelves[] = $userCategoryStoreShelf;
            $userCategoryStoreShelf->setStoreShelf($this);
        }

        return $this;
    }

    public function removeUserCategoryStoreShelf(UserCategoryStoreShelf $userCategoryStoreShelf): self
    {
        if ($this->userCategoryStoreShelves->contains($userCategoryStoreShelf)) {
            $this->userCategoryStoreShelves->removeElement($userCategoryStoreShelf);
            // set the owning side to null (unless already changed)
            if ($userCategoryStoreShelf->getStoreShelf() === $this) {
                $userCategoryStoreShelf->setStoreShelf(null);
            }
        }

        return $this;
    }
}

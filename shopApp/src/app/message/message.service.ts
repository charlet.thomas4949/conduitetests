import { Injectable } from '@angular/core';

import { MatSnackBar } from '@angular/material/snack-bar';
import { MessageComponent } from "./message.component"
@Injectable({
  providedIn: 'root'
})
export class MessageService {

  constructor(private _snackBar: MatSnackBar) { }

  durationInSeconds = 5;

  showMessage(message: string, status?: string) {
    var data = { message: message, status: status }
    this._snackBar.openFromComponent(MessageComponent, {
      duration: this.durationInSeconds * 1000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
      data: data
    });
  }
}

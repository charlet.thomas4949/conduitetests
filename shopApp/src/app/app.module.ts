import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

// components
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { ArticlesComponent } from './articles/articles.component';
import { ShelvesComponent } from './shelves/shelves.component';
import { CategoriesComponent } from './categories/categories.component';
import { StaffComponent } from './staff/staff.component';
import { HeaderComponent } from './header/header.component';

import { MaterialModule } from './material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HttpClientModule } from '@angular/common/http';

import { MessageComponent } from './message/message.component';
import { ShopsComponent } from './shops/shops.component';
import { ArticleComponent, DialogConfirmDeleteProduct } from './articles/article/article.component';
import { AddUpdateArticleComponent } from "./articles/add-update-article/add-update-article.component";
import { ShelveComponent, DialogConfirmDeleteSoreShelf } from './shelves/shelve/shelve.component';
import { CategoryComponent, DialogConfirmDeleteCategory } from './categories/category/category.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ArticlesComponent,
    ShelvesComponent,
    CategoriesComponent,
    StaffComponent,
    HeaderComponent,
    MessageComponent,
    ShopsComponent,
    ArticleComponent,
    AddUpdateArticleComponent,
    DialogConfirmDeleteProduct,
    ShelveComponent,
    DialogConfirmDeleteSoreShelf,
    CategoryComponent,
    DialogConfirmDeleteCategory
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
  ],
  entryComponents: [MessageComponent,
    AddUpdateArticleComponent,
    DialogConfirmDeleteProduct,
    DialogConfirmDeleteSoreShelf,
    DialogConfirmDeleteCategory
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

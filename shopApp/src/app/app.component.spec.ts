import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { LoginComponent } from './login/login.component';
import { ArticlesComponent } from './articles/articles.component';
import { ShelvesComponent } from './shelves/shelves.component';
import { CategoriesComponent } from './categories/categories.component';
import { StaffComponent } from './staff/staff.component';
import { HeaderComponent } from './header/header.component';

import { MaterialModule } from './material.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HttpClientModule }    from '@angular/common/http';

import { MessageComponent } from './message/message.component';
import { ShopsComponent } from './shops/shops.component';
import { ArticleComponent, DialogConfirmDeleteProduct } from './articles/article/article.component';
import { AddUpdateArticleComponent } from "./articles/add-update-article/add-update-article.component";
import { ShelveComponent, DialogConfirmDeleteSoreShelf } from './shelves/shelve/shelve.component';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        LoginComponent,
        ArticlesComponent,
        ShelvesComponent,
        CategoriesComponent,
        StaffComponent,
        HeaderComponent,
        MessageComponent,
        ShopsComponent,
        ArticleComponent,
        AddUpdateArticleComponent,
        DialogConfirmDeleteProduct,
        ShelveComponent,
        DialogConfirmDeleteSoreShelf
      ],
      imports: [
        RouterTestingModule,
        BrowserModule,
        AppRoutingModule,
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        HttpClientModule,
      ],
      providers: []
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'shopApp'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('shopApp');
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.content span').textContent).toContain('shopApp app is running!');
  });
});

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from "../auth.service"
import { MessageService } from "../message/message.service"
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  email = '';
  password = "";

  constructor(private authService: AuthService, private messageService: MessageService, private router: Router) {
    if (this.authService.isAuthenticated()) {
      this.router.navigate(['/articles']);
    }
  }

  ngOnInit() {
  }

  login() {
    this.authService.login(this.email, this.password).subscribe();
  }
}
import { Component, OnInit, Inject, ChangeDetectorRef } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable, of, from, Subject } from 'rxjs';

import { ArticleInInvetories } from "src/model/articleInInventories";
import { StoreShelf } from "src/model/storeShelf";
import { Article } from "src/model/article";
import { Store } from "src/model/store"

import { StoresService } from "src/app/servicesAPI/stores/stores.service";
import { ArticlesService } from "src/app/servicesAPI/articles/articles.service";
import { StoreShelfsService } from "src/app/servicesAPI/storeShelfs/store-shelfs.service";
import { MessageService } from "src/app/message/message.service";

export interface DialogData {
  title: string;
  action: string;
}

@Component({
  selector: 'app-add-update-article',
  templateUrl: './add-update-article.component.html',
  styleUrls: ['./add-update-article.component.scss']
})

export class AddUpdateArticleComponent implements OnInit {

  numberItems : number;
  price: number;
  articleInInvetories : ArticleInInvetories; // new or update object
  articles: Article[];
  stores : Store[];
  storeShelfs : StoreShelf[];
  selectedStoreShelfs : StoreShelf[];

  selected_article : Article;
  selected_store : Store;
  selected_storeShelf : StoreShelf;

  constructor(
    public dialogRef: MatDialogRef<AddUpdateArticleComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private storesService : StoresService,
    private articlesService : ArticlesService,
    private storeShelfsService : StoreShelfsService,
    private cdr: ChangeDetectorRef,
    private messageService : MessageService
    ) { 
    
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.getAllArticles();
    this.getAllStores();
    this.getAllStoreShelfs();
    this.getAllStoreShelfs();
  }

  getAllArticles() {
    this.articlesService.getArticles().subscribe((response: Article[]) => {
      this.articles = response;
    })
  }

  getAllStores() {
    this.storesService.getStores().subscribe((response: Store[]) => {
      this.stores = response;
    })
  }

  getAllStoreShelfs() {
    this.storeShelfsService.getStoreShelf().subscribe((response: StoreShelf[]) => {
      this.storeShelfs = response;
      this.selectedStoreShelfs = this.storeShelfs;
      this.cdr.detectChanges();
    })
  }

  changeStore() {
    var new_storeShelf: StoreShelf[] = [];

    this.storeShelfs.forEach(storeShelf => {
      if (storeShelf.store.id == this.selected_store.id) {
        new_storeShelf.push(storeShelf);
      }
    })

    this.selectedStoreShelfs = new_storeShelf;
    this.cdr.detectChanges();
  }


  send() {
    if(this.data.action == "add")
      this.addArticle();
  }

  addArticle() {
    this.messageService.showMessage("La fonctionnalité d'ajout n'est actuellement pas fonctionnelle.", "alert");
    /** 
    this.articlesService.addInvetory(this.numberItems, this.price, this.selected_article, this.selected_storeShelf).subscribe(response => {
      console.log(response);
    })
    */
  }
}

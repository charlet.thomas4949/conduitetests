import { Component, OnInit, Inject } from '@angular/core';
import { Observable, of, from, Subject } from 'rxjs';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { ArticlesService } from "src/app/servicesAPI/articles/articles.service";
import { StoresService } from "src/app/servicesAPI/stores/stores.service"
import { ArticleInInvetories } from "src/model/articleInInventories";
import { Store } from "src/model/store";

import { AddUpdateArticleComponent } from "./add-update-article/add-update-article.component";

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.scss']
})
export class ArticlesComponent implements OnInit {

  articles: ArticleInInvetories[];
  articlesFilter = new Subject<ArticleInInvetories[]>();
  stores: Store[];
  selectedStore = "0";

  constructor(private articlesService: ArticlesService,
    private storesService: StoresService,
    public dialog: MatDialog,
    ) {

  }

  ngOnInit() {
    this.getAllArticles();
    this.getAllStores();
  }

  getAllArticles() {
    this.articlesService.getInventories().subscribe((response: ArticleInInvetories[]) => {
      this.articles = response;
      this.articlesFilter.next(response);
    })
  }

  getAllStores() {
    this.storesService.getStores().subscribe((response: Store[]) => {
      this.stores = response;
    })
  }

  filtreWithStore() {
    var new_article_list: ArticleInInvetories[] = [];

    if (this.selectedStore == "0") {
      new_article_list = this.articles;
    }
    else {
      this.articles.forEach(article => {
        if (article.storeShelf.store.id == Number(this.selectedStore)) {
          new_article_list.push(article);
        }
      })
    }

    this.articlesFilter.next(new_article_list);
  }

  addArticle(): void {
    const dialogRef = this.dialog.open(AddUpdateArticleComponent, {
      width: '25em',
      data: {title: "Ajouter un article", action: "add"}
    });
  }
}

import { Component, OnInit, Input, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { ArticleInInvetories } from "src/model/articleInInventories";
import { AuthService } from "src/app/auth.service";
import { MessageService } from "src/app/message/message.service";
import { ArticlesService } from "src/app/servicesAPI/articles/articles.service";

export interface DialogData {
  produit: string;
  id : string;
}

@Component({
  selector: '[app-article]',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit {

  @Input() article: ArticleInInvetories;

  constructor(private authService: AuthService,
    private messageService: MessageService,
    public dialog: MatDialog) { }

  ngOnInit() {
  }

  getCategories(): string {
    var categories = "";
    var nb_items = 0;
    this.article.item.categories.forEach(category => {
      if (nb_items > 0)
        categories += ", "
      categories += category.name;
      nb_items++
    })
    return categories;
  }

  getPermission(): boolean {
    var response = false;
    var user = this.authService.user;

    switch (user.role.name) {
      case "PDG":
        response = true;
        break;
      case "Chef de Magasin":
        if (user.store.id == this.article.storeShelf.store.id)
          response = true
        break;
      case "Responsable de Rayon":
        user.categoryStoreShelves.forEach(item => {
          if (item.storeShelf.id == this.article.storeShelf.id && user.store.id == this.article.storeShelf.store.id)
            response = true;
        })
        break;
    }
    return response;
  }

  updateArticle() {
    this.messageService.showMessage("La fonctionnalité modifier n'est actuellement pas fonctionnelle.", "alert");
  }

  showBoxTodeleteArticle() {
    const dialogRef = this.dialog.open(DialogConfirmDeleteProduct, {
      width: '250px',
      data: {
        id : this.article.id,
        produit: this.article.numberItems + " * " + this.article.item.name + " (" + this.article.item.refCode + ") dans le rayon " +
          this.article.storeShelf.name + " à " + this.article.storeShelf.store.name
      }
    });
  }
}


@Component({
  selector: 'dialog-confirm-delete',
  templateUrl: 'dialog-confirm-delete.html',
})
export class DialogConfirmDeleteProduct {

  constructor(
    public dialogRef: MatDialogRef<DialogConfirmDeleteProduct>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private messageService: MessageService,
    private articlesService : ArticlesService) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  deleteArticle() {
    this.articlesService.deleteInventory(this.data.id).subscribe(result => {
      this.messageService.showMessage("L'article a été supprimé avec succès","success");
    });
    
  }

}
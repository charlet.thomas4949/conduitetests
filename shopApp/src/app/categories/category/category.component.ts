import { Component, OnInit, Input, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { ArticleInInvetories } from "src/model/articleInInventories";
import { AuthService } from "src/app/auth.service";
import { MessageService } from "src/app/message/message.service";
import { StoreShelfsService } from "src/app/servicesAPI/storeShelfs/store-shelfs.service";

import { Category } from 'src/model/category';
import { CategoriesService } from 'src/app/servicesAPI/categories/categories.service';

export interface DialogData {
  category: string;
  id : string;
}

@Component({
  selector: '[app-category]',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {

  @Input() category: Category;

  constructor(private authService: AuthService,
    private messageService: MessageService,
    public dialog: MatDialog) { }

  ngOnInit() {
  }

  getPermission(): boolean {
    var response = false;
    var user = this.authService.user;

    switch (user.role.name) {
      case "PDG":
        response = true;
        break;
      case "Chef de Magasin":
          response = true
        break;
    }
    return response;
  }

  updateCategory() {
    this.messageService.showMessage("La fonctionnalité modifier n'est actuellement pas fonctionnelle.", "alert");
  }

  showBoxTodeleteCategory() {
    const dialogRef = this.dialog.open(DialogConfirmDeleteCategory, {
      width: '250px',
      data: {
        id : this.category.id,
        produit: this.category.name 
      }
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

}

@Component({
  selector: 'dialog-confirm-delete',
  templateUrl: 'dialog-confirm-delete-category.html',
})
export class DialogConfirmDeleteCategory {

  constructor(
    public dialogRef: MatDialogRef<DialogConfirmDeleteCategory>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private messageService: MessageService,
    private categoriesService : CategoriesService) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  delete() {
    this.categoriesService.deleteCategory(this.data.id).subscribe(result => {
      this.messageService.showMessage("La catégorie a été supprimé avec succès","success");
    });
    
  }

}
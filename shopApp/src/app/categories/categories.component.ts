import { Component, OnInit, Inject } from '@angular/core';
import { Observable, of, from, Subject } from 'rxjs';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { ArticlesService } from "src/app/servicesAPI/articles/articles.service";
import { StoresService } from "src/app/servicesAPI/stores/stores.service"
import { ArticleInInvetories } from "src/model/articleInInventories";
import { Store } from "src/model/store";
import { Category } from "src/model/category";
import { CategoriesService } from '../servicesAPI/categories/categories.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {

  categories: Category[];
  categoriesFilter = new Subject<Category[]>();

  constructor(private articlesService: ArticlesService,
    private categoriesService: CategoriesService,
    public dialog: MatDialog,
  ) {
  }

  ngOnInit() {
    this.getAllCategories();
  }

  getAllCategories() {
    this.categoriesService.getCategories().subscribe((response: Category[]) => {
      this.categories = response;
      this.categoriesFilter.next(response);
    })
  }
}

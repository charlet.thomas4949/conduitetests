import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from "../auth.service";
import { MessageService } from "../message/message.service";

import { CategoryStoreShelve } from "src/model/categoryStoreShelve"

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Input() activePage: string;
  firstName = "";
  lastName = "";
  role = "";

  constructor(private authService: AuthService, private messageService: MessageService, private router: Router) {
    if (!this.authService.isAuthenticated()) {
      this.messageService.showMessage("Cette page necessite une connexion.", "alert");
      this.router.navigate(['/']);
    }

    var user = this.authService.user;
    this.firstName = user.firstName;
    this.lastName = user.lastName;

    switch (user.role.name) {
      case "PDG":
        this.role = "PDG";
        break;
      case "Chef de Magasin":
        this.role = "Chef de magasin de " + user.store.name;
        break;
      case "Responsable de Rayon":
        var rayons = "";
        var nb_rayons = 0;
        user.categoryStoreShelves.forEach((value : CategoryStoreShelve) => {
          if(nb_rayons >0 )
            rayons +=","
          rayons += " " + value.storeShelf.name;
          nb_rayons++;
        });      
        this.role = "Chef du rayon" + rayons + " à " + user.store.name;
        break;
    }
  }

  ngOnInit() {
  }

  logOut() {
    this.authService.logout();
  }
}

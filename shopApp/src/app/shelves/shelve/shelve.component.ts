import { Component, OnInit, Input, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { ArticleInInvetories } from "src/model/articleInInventories";
import { AuthService } from "src/app/auth.service";
import { MessageService } from "src/app/message/message.service";
import { StoreShelfsService } from "src/app/servicesAPI/storeShelfs/store-shelfs.service";

import { StoreShelf  } from "src/model/storeShelf";

export interface DialogData {
  storeShelf: string;
  id : string;
}

@Component({
  selector: '[app-shelve]',
  templateUrl: './shelve.component.html',
  styleUrls: ['./shelve.component.scss']
})
export class ShelveComponent implements OnInit {

  @Input() storeShelf: StoreShelf;

  constructor(private authService: AuthService,
    private messageService: MessageService,
    public dialog: MatDialog) { }

  ngOnInit() {
  }

  getCategories(): string {
    var categories = "";
    var nb_items = 0;
    // TODO : Get Categories from API
    /** 
    this.storeShelf.categories.forEach(category => {
      if (nb_items > 0)
        categories += ", "
      categories += category.name;
      nb_items++
    })
    */
    return categories;
  }

  getPermission(): boolean {
    var response = false;
    var user = this.authService.user;

    switch (user.role.name) {
      case "PDG":
        response = true;
        break;
      case "Chef de Magasin":
        if (user.store.id == this.storeShelf.store.id)
          response = true
        break;
    }
    return response;
  }

  updateArticle() {
    this.messageService.showMessage("La fonctionnalité modifier n'est actuellement pas fonctionnelle.", "alert");
  }

  showBoxTodeleteArticle() {
    const dialogRef = this.dialog.open(DialogConfirmDeleteSoreShelf, {
      width: '250px',
      data: {
        id : this.storeShelf.id,
        produit: this.storeShelf.name + " (" + this.storeShelf.codeShelf + ") " 
         + " à " + this.storeShelf.store.name
      }
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

}

@Component({
  selector: 'dialog-confirm-delete',
  templateUrl: 'dialog-confirm-delete-storeShelf.html',
})
export class DialogConfirmDeleteSoreShelf {

  constructor(
    public dialogRef: MatDialogRef<DialogConfirmDeleteSoreShelf>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private messageService: MessageService,
    private storeShelfsService : StoreShelfsService) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  delete() {
    this.storeShelfsService.deleteStoreShelf(this.data.id).subscribe(result => {
      this.messageService.showMessage("Le rayon a été supprimé avec succès","success");
    });
    
  }

}

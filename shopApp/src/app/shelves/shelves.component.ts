import { Component, OnInit, Inject } from '@angular/core';
import { Observable, of, from, Subject } from 'rxjs';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { ArticlesService } from "src/app/servicesAPI/articles/articles.service";
import { StoresService } from "src/app/servicesAPI/stores/stores.service"
import { ArticleInInvetories } from "src/model/articleInInventories";
import { Store } from "src/model/store";
import { Article } from "src/model/article";
import { StoreShelf } from 'src/model/storeShelf';
import { StoreShelfsService } from '../servicesAPI/storeShelfs/store-shelfs.service';

@Component({
  selector: 'app-shelves',
  templateUrl: './shelves.component.html',
  styleUrls: ['./shelves.component.scss']
})
export class ShelvesComponent implements OnInit {

  storeShelves: StoreShelf[];
  storeShelvesFilter = new Subject<StoreShelf[]>();
  stores: Store[];
  selectedStore = "0";

  constructor(private storeShelfsService: StoreShelfsService,
    private storesService: StoresService,
    public dialog: MatDialog,
    ) {

  }

  ngOnInit() {
    this.getAllStoreShelves();
    this.getAllStores();
  }

  getAllStoreShelves() {
    this.storeShelfsService.getStoreShelf().subscribe((response: StoreShelf[]) => {
      this.storeShelves = response;
      this.storeShelvesFilter.next(response);
      console.log(response);
    })
  }

  getAllStores() {
    this.storesService.getStores().subscribe((response: Store[]) => {
      this.stores = response;
    })
  }

  filtreWithStore() {
    var new_storeShelfs_list: StoreShelf[] = [];

    if (this.selectedStore == "0") {
      new_storeShelfs_list = this.storeShelves;
    }
    else {
      this.storeShelves.forEach(storeShelf => {
        if (storeShelf.store.id == Number(this.selectedStore)) {
          new_storeShelfs_list.push(storeShelf);
        }
      })
    }

    this.storeShelvesFilter.next(new_storeShelfs_list);
  }

}

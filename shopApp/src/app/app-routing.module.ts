import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component'
import { ArticlesComponent } from './articles/articles.component'
import { ShelvesComponent } from './shelves/shelves.component'
import { CategoriesComponent } from './categories/categories.component'
import { StaffComponent } from './staff/staff.component'
import { ShopsComponent } from './shops/shops.component'

const routes: Routes = [
  { path : "", component: LoginComponent},
  { path : "articles", component: ArticlesComponent},
  { path : "shelves", component: ShelvesComponent},
  { path : "categories", component: CategoriesComponent},
  { path : "shops", component: ShopsComponent},
  { path : "staff", component: StaffComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

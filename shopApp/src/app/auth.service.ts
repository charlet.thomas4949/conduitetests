import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { User } from '../model/user';

import { MessageService } from "./message/message.service";
import { UsersService } from "./servicesAPI/users/users.service";

import { environment } from "src/environments/environment";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private apiUrl = environment.urlAPI;  // URL to web api

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  // informations about user
  user: User = null;

  constructor(private http: HttpClient,
    private messageService: MessageService,
    private router: Router,
    private usersService: UsersService) { }

  isAuthenticated() {
    return (this.user != null);
  }

  /**
   * login
   * @param email 
   * @param password 
   */
  login(email, password): Observable<Object> {

    return this.http.post<any>(this.apiUrl + "login" + "?email=" + email + "&password=" + password, this.httpOptions).pipe(
      tap((response: any) => {

        this.usersService.getUserById(response).subscribe((user : User) => {
          if (user != null) {
            this.user = user;
            this.messageService.showMessage("Connexion en tant que " + this.user.firstName + " " + this.user.lastName, "success");
            this.router.navigate(['/articles'])
          }
        })
      }
      ),
      catchError(this.handleError<string>('login'))
    );
  }

  logout() {
    this.user = null;
    this.messageService.showMessage("Vous êtes désormais déconnectés.", "success");
    this.router.navigate(['/']);
  }

  /**
 * Handle Http operation that failed.
 * Let the app continue.
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.messageService.showMessage(error.error.text, "error");

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}

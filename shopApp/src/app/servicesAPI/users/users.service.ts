import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { User } from 'src/model/user'

import { MessageService } from "src/app/message/message.service"
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  private apiUrl = environment.urlAPI;  // URL to web api

  constructor(private http: HttpClient,
    private messageService: MessageService) {
  }

  /**
  * get Users
  */
  getUsers() {

  }

  /**
   * get User by ID
   */
  getUserById(id: number): Observable<User> {
    return new Observable((observer) => {
      this.http.get<any>(this.apiUrl + "users/" + id).pipe(
        catchError(this.handleError<string>('getUserById'))
      ).subscribe((response: any) => { 
        var user = new User();
        user.id = response.id;
        user.email = response.email;
        user.firstName = response.firstName;
        user.lastName = response.lastName;
        user.matricule = response.userId;
        user.birthday = new Date(response.birthday);
        user.role = response.role;
        user.store = response.stores;
        user.gender = response.gender;
        user.categoryStoreShelves = response.userCategoryStoreShelves;

        observer.next(user);
        observer.complete();
      });
    });
  }


/**
 * get role by ID
 */
getRoleByID(id: number) {
  return this.http.get<any>(this.apiUrl + "roles/" + id).pipe(
    tap((response: any) => {


    }
    ),
    catchError(this.handleError<string>('login'))
  );
}

/**
 * get gender by Id
 */
getGenderById() {

}

  /**
 * Handle Http operation that failed.
 * Let the app continue.
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */
  private handleError<T>(operation = 'operation', result ?: T) {
  return (error: any): Observable<T> => {

    // TODO: send the error to remote logging infrastructure
    console.error(error); // log to console instead

    // TODO: better job of transforming error for user consumption
    this.messageService.showMessage(error.error.text, "error");

    // Let the app keep running by returning an empty result.
    return of(result as T);
  };
}

}

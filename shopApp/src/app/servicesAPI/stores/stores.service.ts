import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Store } from 'src/model/store'

import { MessageService } from "src/app/message/message.service"
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: 'root'
})
export class StoresService {

  private apiUrl = environment.urlAPI;  // URL to web api

  constructor(private http: HttpClient,
    private messageService: MessageService) {
  }

/**
 * get stores
 */
getStores(): Observable<Store[]> {
  return new Observable((observer) => {
    this.http.get<any>(this.apiUrl + "stores").pipe(
      catchError(this.handleError<string>('getStores'))
    ).subscribe((response: Store[]) => {
      observer.next(response);
      observer.complete();
    });
  });
}


  /**
* Handle Http operation that failed.
* Let the app continue.
* @param operation - name of the operation that failed
* @param result - optional value to return as the observable result
*/
private handleError<T>(operation = 'operation', result?: T) {
  return (error: any): Observable<T> => {

    // TODO: send the error to remote logging infrastructure
    console.error(error); // log to console instead

    // TODO: better job of transforming error for user consumption
    this.messageService.showMessage(error.error.text, "error");

    // Let the app keep running by returning an empty result.
    return of(result as T);
  };
}

}

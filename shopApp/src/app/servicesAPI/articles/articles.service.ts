import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { ArticleInInvetories } from 'src/model/articleInInventories';
import { Article } from 'src/model/article'

import { MessageService } from "src/app/message/message.service";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ArticlesService {

  private apiUrl = environment.urlAPI;  // URL to web api

  constructor(private http: HttpClient,
    private messageService: MessageService) {
  }


  /**
 * get inventories
 */
  getInventories(): Observable<ArticleInInvetories[]> {
    return new Observable((observer) => {
      this.http.get<any>(this.apiUrl + "inventories").pipe(
        catchError(this.handleError<string>('getInventories'))
      ).subscribe((response: ArticleInInvetories[]) => {
        observer.next(response);
        observer.complete();
      });
    });
  }

  /**
* get inventories
*/
  addInvetory(numberItems, price, item: Article, storeShelf): Observable<any> {

    var httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };

    var categories = [];

    item.categories.forEach(category => {
      var category_attibutes = {
        name: category.name,
        codeCategory: category.id,
        items: [null],
        userCategoryStoreShelves: [null],
        inventories: [null]
      }
      categories.push(category_attibutes);
    });

    var params = {
      numberItems: numberItems,
      price: price,
      item: {
        refCode: item.refCode,
        name: item.name,
        description: item.description,
        categories: categories
      },
      storeShelf: {
        numShelf: storeShelf.numShelf,
        codeShelf: storeShelf.codeShelf,
        name: storeShelf.name,
        store: {
          codeStore: storeShelf.store.codeStore,
          name: storeShelf.store.name
        }
      }
    };
    var json = JSON.stringify(params);
    console.log(params);
    console.log(json);

    return new Observable((observer) => {
      this.http.post<any>(this.apiUrl + "inventories", json, httpOptions).pipe(
        catchError(this.handleError<string>('addInvetory'))
      ).subscribe((response: any) => {
        console.log(response);
      });
    });
  }

  /**
 * delete inventory
 */
  deleteInventory(id: string): Observable<any> {
    return this.http.delete<any>(this.apiUrl + "inventories/" + id).pipe(
      catchError(this.handleError<string>('deleteInventory'))
    );
  }

  /**
 * get all types of articles
 */
  getArticles(): Observable<Article[]> {
    return new Observable((observer) => {
      this.http.get<any>(this.apiUrl + "items").pipe(
        catchError(this.handleError<string>('getArticles'))
      ).subscribe((response: Article[]) => {
        observer.next(response);
        observer.complete();
      });
    });
  }

  /**
* Handle Http operation that failed.
* Let the app continue.
* @param operation - name of the operation that failed
* @param result - optional value to return as the observable result
*/
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.messageService.showMessage(error.error.text, "error");

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}

import { TestBed } from '@angular/core/testing';

import { StoreShelfsService } from './store-shelfs.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { StoreShelf } from 'src/model/storeShelf'

import { MessageService } from "src/app/message/message.service"
import { environment } from "src/environments/environment";

describe('StoreShelfsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StoreShelfsService = TestBed.get(StoreShelfsService);
    expect(service).toBeTruthy();
  });
});

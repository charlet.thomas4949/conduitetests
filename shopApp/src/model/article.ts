import { Category } from "./category"

export class Article {
    id: number;
    refCode: string;
    name: string;
    description: string;
    categories: Category[];
  }
import { Category } from "./category"
import { StoreShelf } from "./storeShelf"

export class CategoryStoreShelve {
    id: number;
    category : Category
    storeShelf: StoreShelf;
  }
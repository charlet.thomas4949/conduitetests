import { Article } from "./article"
import { StoreShelf } from './storeShelf';

export class ArticleInInvetories {
    id: number;
    numberItems: number;
    price: number;
    item: Article;
    storeShelf: StoreShelf;
  }
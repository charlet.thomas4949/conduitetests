import { Store } from './store';

export class StoreShelf {
    id: number;
    name: string;
    store: Store;
    numShelf: number;
    codeShelf: string;
  }
import { Role } from "./role"
import { Store } from "./store"
import { Gender } from "./gender"
import { CategoryStoreShelve } from "./categoryStoreShelve"

export class User {
  id: number;
  matricule: string;
  name: string;
  birthday: Date;
  email: string;
  firstName: string;
  lastName: string;
  role : Role;
  store : Store;
  gender: Gender;
  categoryStoreShelves: CategoryStoreShelve[];
}